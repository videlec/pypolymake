import unittest

import polymake

class TestSubdivisionOfPoints(unittest.TestCase):
    def test_doc_example(self):
        c = polymake.SubdivisionOfPoints(POINTS=polymake.cube(2).VERTICES, WEIGHTS=[0,0,0,1])
        mc = c.MAXIMAL_CELLS
        self.assertEqual(mc.rows(), 2)
        self.assertEqual(mc.cols(), 4)
        self.assertEqual(str(mc), '{0 1 2}\n{1 2 3}\n')

if __name__ == '__main__':
    unittest.main()

