#!/usr/bin/env bash

TESTS="test_coercion.py test_fan.py test_integers.py test_matrix.py test_polytope_quadratic_extension.py test_polytope_rational.py test_quadratic_extensions.py test_rationals.py test_sage.py test_vector.py"

export PYTHONPATH="../"
if [ -z ${PYTHON+x} ]
then
    PYTHON="python"
fi

for test in ${TESTS}
do
    ${PYTHON} ${test}
done

