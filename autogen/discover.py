import polymake
from pm_types import pm_types
from polymake.perl_object import get_properties, get_methods

perl_types = [v["perl"] for v in pm_types().values()]
print("Known small objects")
print("\n".join(perl_types))

objects = []
c = polymake.cube(3)
objects.append(("cube(3)", c))
objects.append(("cube(3)->GRAPH", c.GRAPH))
objects.append(("cube(3)->HASSE_DIAGRAM", c.HASSE_DIAGRAM))
objects.append(("cube(3)->MINKOWSKI_CONE", c.MINKOWSKI_CONE))
objects.append(("cube(3)->TRIANGULATION", c.TRIANGULATION))
objects.append(("cube(3)->TRIANGULATION->BOUNDARY", c.TRIANGULATION.BOUNDARY))

c2 = polymake.regular_120_cell()
objects.append(("regular_120_cell()", c2))
objects.append(("regular_120_cell()->GROUP", c2.GROUP))
objects.append(("regular_120_cell()->GROUP->FACETS_ACTION", c2.GROUP.FACETS_ACTION))



known_big = [
    b"Cone<Rational>",
    b"Polytope<Rational>",
    b"SimplicialComplex",
    b"Lattice<BasicDecoration, Nonsequential>",
    b"GeometricSimplicialComplex<Rational>",
    b"PermutationAction<Int, Rational>",
    b"Group",
    ]

missing = []
new_types = set()

for name, obj in objects:
    print(name)
    print(obj)
    print("properties")
    for prop,typ in get_properties(obj).items():
        if typ.decode('ascii') not in perl_types and typ not in known_big:
            print(prop, typ)
            if not typ:
                missing.append((name, prop))
            else:
                new_types.add(typ)
    print("methods")
    for prop,typ in get_methods(obj).items():
        if typ.decode('ascii') not in perl_types and typ not in known_big:
            print(prop, typ)
            if not typ:
                missing.append((name, prop))
            else:
                new_types.add(typ)
    print()
print()
print("Missing:")
for m in missing:
    print(" ", m)
print()
print("New types")
for m in new_types:
    print(" ", m)


