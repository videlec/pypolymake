import polymake

c = polymake.cube(3)

q = c.QUOTIENT_SPACE
# polymake:  WARNING: available properties insufficient to compute 'QUOTIENT_SPACE'
# terminate called after throwing an instance of 'pm::perl::exception'
# what():  property QUOTIENT_SPACE not created as expected at
# /usr/local/share/polymake/perllib/Polymake/Core/Object.pm line 1537.


t = c.TRIANGULATION.INTERSECTION_FORM
# polymake:  WARNING: rule INTERSECTION_FORM : CYCLES, COCYCLES failed: intersection_form: Dimension 3 not
# divisible by 4 at /usr/local/share/polymake/apps/topaz/rules/common.rules line 238.
# terminate called after throwing an instance of 'pm::perl::exception'
# what():  no more rules available to compute 'INTERSECTION_FORM'
# Abandon (core dumped)
